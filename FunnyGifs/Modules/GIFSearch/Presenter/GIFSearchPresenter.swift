//
//  GIFSearchPresenter.swift
//  FunnyGifs
//
//  Created by Швецов Денис on 12/09/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

import Foundation

protocol GIFSearchViewOutput {
    var viewModels: [GIFViewModel] { get }
    func viewIsReady()
    func didScrollToNextPage()
}

class GIFSearchPresenter {
    
    weak var view: GIFSearchViewInput!
    
    var viewModels = [GIFViewModel]()
    
    private let router: GIFSearchRouter.Routes
    private let service: GIFSearchServiceInput
    
    private var search = ""
    
    private var state = PaginationState(limit: 20)
    
    init(router: GIFSearchRouter.Routes, service: GIFSearchServiceInput) {
        self.router = router
        self.service = service
    }
}

extension GIFSearchPresenter: GIFSearchModuleInput {
    
    func search(_ text: String) {
        guard search != text, !text.isEmpty else { return }
        search = text
        viewModels.removeAll()
        view.reloadData()
        view.resetContentOffset()
        view.setFooterState(.loading)
        state.reset()
        service.requestGIFs(search: text, limit: state.limit, offset: 0, force: false)
    }
}

extension GIFSearchPresenter: GIFSearchViewOutput {
    
    func viewIsReady() {
        
    }
    
    func didScrollToNextPage() {
        guard state.canLoadingMore else { return }
        state.isLoadingMore = true
        view.setFooterState(.loading)
        service.requestGIFs(search: search, limit: state.limit, offset: state.offset, force: false)
    }
}

extension GIFSearchPresenter: GIFSearchServiceOutput {
    
    func requestGIFs(didFinishWith gifs: [GIFViewModel], pagination: Pagination) {
        if pagination.offset == 0 {
            viewModels = gifs
        } else {
            viewModels.append(contentsOf: gifs)
        }
        state.pagination = pagination
        state.isLoadingMore = false
        state.isEmpty = viewModels.isEmpty
        
        view.reloadData()
        view.setFooterState(viewModels.isEmpty ? .text : .none)
    }
    
    func requestGIFs(didFailWith error: ApiError) {
        view.scrollToLastRow(if: state.isLoadingMore)
        router.openAlertModule(error: error, handler: { self.state.isLoadingMore = false })
        view.setFooterState(viewModels.isEmpty ? .text : .none)
    }
}
