//
//  GIFSearchService.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 13/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Result
import SwiftyJSON

protocol GIFSearchServiceInput: AnyObject {
    func requestGIFs(search: String, limit: Int, offset: Int, force: Bool)
}

protocol GIFSearchServiceOutput: AnyObject {
    func requestGIFs(didFinishWith gifs: [GIFViewModel], pagination: Pagination)
    func requestGIFs(didFailWith error: ApiError)
}

class GIFSearchService: GIFSearchServiceInput {
    
    weak var output: GIFSearchServiceOutput?
    
    private let apiProvider: ApiProvider<GiphyTarget>
    private let storage: Storage
    private let throttler: Throttler
    
    private var lastSearch = ""
    
    init(apiProvider: ApiProvider<GiphyTarget>, storage: Storage, throttler: Throttler = Throttler(delay: 0.5)) {
        self.apiProvider = apiProvider
        self.storage = storage
        self.throttler = throttler
    }
    
    func requestGIFs(search: String, limit: Int, offset: Int, force: Bool) {
        lastSearch = search
        guard !search.isEmpty else { return }
        let completion = { [weak self] (result: Result<JSON, ApiError>) in
            guard let self = self else { return }
            switch result {
            case let .success(response):
                let data = response["data"]
                let pagination = Pagination(json: response["pagination"])
                self.storage.storeObjects(GIF.self, from: data, force: force) { result in
                    switch result {
                    case let .success(viewModels):
                        guard self.lastSearch == search else { return }
                        self.output?.requestGIFs(didFinishWith: viewModels, pagination: pagination)
                    case let .failure(error):
                        self.output?.requestGIFs(didFailWith: error)
                    }
                }
            case let .failure(error):
                self.output?.requestGIFs(didFailWith: error)
            }
        }
        throttler.throttle { [weak self] in
            guard let self = self else { return }
            self.apiProvider.request(.search(search, limit: limit, offset: offset), completion: completion)
        }
    }
}


