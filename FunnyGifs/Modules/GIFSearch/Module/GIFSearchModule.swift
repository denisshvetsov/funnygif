//
//  GIFSearchModule.swift
//  FunnyGifs
//
//  Created by Швецов Денис on 12/09/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

protocol GIFSearchModuleInput: AnyObject {
    func search(_ text: String)
}

class GIFSearchModule {
    
    var input: GIFSearchModuleInput {
        return presenter
    }
    
    let router: GIFSearchRouter
    let viewController: GIFSearchViewController
    
    private let presenter: GIFSearchPresenter
    
    init(transition: Transition?) {
        let router = GIFSearchRouter()
        let apiProvider = ApiProvider<GiphyTarget>()
        let service = GIFSearchService(apiProvider: apiProvider, storage: Storage())
        let presenter = GIFSearchPresenter(router: router, service: service)
        let viewController = GIFSearchViewController(output: presenter)
        
        service.output = presenter
        presenter.view = viewController
        router.viewController = viewController
        router.openTransition = transition
        
        self.router = router
        self.viewController = viewController
        self.presenter = presenter
    }
}
