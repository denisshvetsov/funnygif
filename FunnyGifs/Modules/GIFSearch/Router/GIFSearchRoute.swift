//
//  GIFSearchRoute.swift
//  FunnyGifs
//
//  Created by Швецов Денис on 12/09/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

protocol GIFSearchRoute {
    func openGIFSearchModule()
}

extension GIFSearchRoute where Self: RouterProtocol {
    
    func openGIFSearchModule() {
    
    }
}
