//
//  GIFSearchViewController.swift
//  FunnyGifs
//
//  Created by Швецов Денис on 12/09/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

protocol GIFSearchViewInput: AnyObject {
    func resetContentOffset()
    func reloadData()
    func setFooterState(_ state: FooterView.State)
    func scrollToLastRow(if isNeeded: Bool)
}

class GIFSearchViewController: UITableViewController {
    
    private let output: GIFSearchViewOutput
    
    private let footerView = FooterView(frame: CGRect(x: 0, y: 0, width: 0, height: 60),
                                        text: "Nothing found")
    
    private var defaultContentOffset: CGPoint? // Because searchResultsController default contentOffset not Point.zero
    
    init(output: GIFSearchViewOutput) {
        self.output = output
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(GIFTableViewCell.self, forCellReuseIdentifier: GIFTableViewCell.reuseIdentifier)
        tableView.separatorStyle = .none
        tableView.tableFooterView = footerView
        tableView.backgroundColor = .black
        tableView.keyboardDismissMode = .onDrag
        tableView.prefetchDataSource = self
        tableView.setSmoothScroll()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard defaultContentOffset == nil else { return }
        defaultContentOffset = tableView.contentOffset
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.viewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GIFTableViewCell.reuseIdentifier, for: indexPath) as! GIFTableViewCell
        cell.setup(model: output.viewModels[indexPath.row])
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return output.viewModels[indexPath.row].cellHeight
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        if deltaOffset < footerView.frame.height {
            output.didScrollToNextPage()
        }
    }
}

extension GIFSearchViewController: GIFSearchViewInput {
    
    func resetContentOffset() {
        tableView.contentOffset = defaultContentOffset ?? CGPoint.zero
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func setFooterState(_ state: FooterView.State) {
        footerView.state = state
    }
    
    func scrollToLastRow(if isNeeded: Bool) {
        let lastRow = tableView.numberOfRows(inSection: 0) - 1
        guard lastRow >= 0, isNeeded else { return }
        let indexPath = IndexPath(row: lastRow, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}

extension GIFSearchViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.compactMap { output.viewModels[$0.row].url }
        urls.forEach { ImageProvider.shared.prefetch(with: $0) }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.compactMap { output.viewModels[$0.row].url }
        urls.forEach { ImageProvider.shared.cancel(with: $0) }
    }
}
