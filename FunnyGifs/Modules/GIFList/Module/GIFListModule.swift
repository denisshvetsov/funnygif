//
//  GIFListModule.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

protocol GIFListModuleInput: AnyObject {
    
}

class GIFListModule {
    
    var input: GIFListModuleInput {
        return presenter
    }
    
    let router: GIFListRouter
    let viewController: GIFListViewController
    
    private let presenter: GIFListPresenter
    
    init(transition: Transition?) {
        let router = GIFListRouter()
        let apiProvider = ApiProvider<GiphyTarget>()
        let service = GIFListService(apiProvider: apiProvider, storage: Storage())
        
        let searchModule = GIFSearchModule(transition: nil)
        
        let presenter = GIFListPresenter(router: router, service: service, searchModule: searchModule.input)
        
        let searchController = UISearchController(searchResultsController: searchModule.viewController)
        let viewController = GIFListViewController(output: presenter, searchController: searchController)
        
        service.output = presenter
        presenter.view = viewController
        router.viewController = viewController
        router.openTransition = transition
        
        self.router = router
        self.viewController = viewController
        self.presenter = presenter
    }
}
