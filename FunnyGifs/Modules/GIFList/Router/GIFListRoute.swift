//
//  GIFListRoute.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

protocol GIFListRoute {
    func openGIFListModule()
}

extension GIFListRoute where Self: RouterProtocol {
    
    func openGIFListModule() {
    
    }
}
