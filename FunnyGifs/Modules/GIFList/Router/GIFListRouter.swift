//
//  GIFListRouter.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

class GIFListRouter: Router<GIFListViewController>, GIFListRouter.Routes {
    
    typealias Routes = AlertRoute
    
}
