//
//  GIFListViewController.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

protocol GIFListViewInput: AnyObject {
    func reloadData()
    func endRefreshing()
    func setFooterState(_ state: FooterView.State)
    func scrollToLastRow(if isNeeded: Bool)
}

class GIFListViewController: UITableViewController {
    
    private let output: GIFListViewOutput
    private let searchController: UISearchController
    
    private let footerView = FooterView(frame: CGRect(x: 0, y: 0, width: 0, height: 60),
                                        text: "Empty")
    
    init(output: GIFListViewOutput, searchController: UISearchController) {
        self.output = output
        self.searchController = searchController
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Trending"
        
        definesPresentationContext = true
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.barStyle = .black
        searchController.searchBar.tintColor = .white
        searchController.searchBar.keyboardAppearance = .dark
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl?.tintColor = .white
        
        tableView.register(GIFTableViewCell.self, forCellReuseIdentifier: GIFTableViewCell.reuseIdentifier)
        tableView.separatorStyle = .none
        tableView.tableFooterView = footerView
        tableView.backgroundColor = .black
        tableView.prefetchDataSource = self
        tableView.setSmoothScroll()
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            tableView.tableHeaderView = searchController.searchBar
            searchController.hidesNavigationBarDuringPresentation = false
            searchController.searchBar.barTintColor = UIColor.translucentBlack
            tableView.refreshControl?.backgroundColor = UIColor.translucentBlack
        }
        
        output.viewIsReady()
    }
    
    @objc private func refresh(_ sender: UIRefreshControl) {
        output.refresh()
    }
    
    // MARK: UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.viewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GIFTableViewCell.reuseIdentifier, for: indexPath) as! GIFTableViewCell
        cell.setup(model: output.viewModels[indexPath.row])
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return output.viewModels[indexPath.row].cellHeight
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        if deltaOffset < footerView.frame.height {
            output.didScrollToNextPage()
        }
    }
}

extension GIFListViewController: GIFListViewInput {
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func endRefreshing() {
        guard tableView.refreshControl?.isRefreshing ?? false else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) { // delay for better animation if request is short
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    func setFooterState(_ state: FooterView.State) {
        footerView.state = state
    }
    
    func scrollToLastRow(if isNeeded: Bool) {
        let lastRow = tableView.numberOfRows(inSection: 0) - 1
        guard lastRow >= 0, isNeeded else { return }
        let indexPath = IndexPath(row: lastRow, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}

extension GIFListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        output.search(searchController.searchBar.text ?? "")
    }
}

extension GIFListViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.compactMap { output.viewModels[$0.row].url }
        urls.forEach { ImageProvider.shared.prefetch(with: $0) }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        let urls = indexPaths.compactMap { output.viewModels[$0.row].url }
        urls.forEach { ImageProvider.shared.cancel(with: $0) }
    }
}
