//
//  GIFTableViewCell.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit
import YYImage

class GIFTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = String(describing: self)
    
    private let gifImageView: YYAnimatedImageView = {
        let imageView = YYAnimatedImageView(frame: CGRect.zero)
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private let gifPlaceholderImageView: UIImageView = { // for prevent flickering on setAnimatedImage
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.backgroundColor = .darkGray
        return imageView
    }()
    
    private var key = ""
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSetup()
    }
    
    private func initSetup() {
        selectionStyle = .none
        contentView.backgroundColor = .black
        contentView.addSubview(gifPlaceholderImageView)
        contentView.addSubview(gifImageView)
    }
    
    func setup(model: GIFViewModel) {
        gifPlaceholderImageView.image = nil
        gifImageView.image = nil
        
        gifPlaceholderImageView.frame = model.imageFrame
        gifImageView.frame = model.imageFrame
        
        key = model.url?.key ?? ""
        
        guard let modelUrl = model.url else { return }
        ImageProvider.shared.get(with: modelUrl) { [weak self] image, resource in
            guard let self = self, self.key == resource.key else { return }
            self.gifPlaceholderImageView.image = image?.frames.first?.image
            self.gifImageView.alpha = 0
            self.gifImageView.image = image
            UIView.animate(withDuration: 0.2, animations: { // UIView.transition broken with YYAnimatedImageView
                self.gifImageView.alpha = 1
            })
        }
    }
}
