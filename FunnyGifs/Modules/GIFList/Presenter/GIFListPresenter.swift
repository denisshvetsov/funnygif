//
//  GIFListPresenter.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright (c) 2019 Denis Shvetsov. All rights reserved.
//

import Foundation

protocol GIFListViewOutput {
    var viewModels: [GIFViewModel] { get }
    func viewIsReady()
    func refresh()
    func didScrollToNextPage()
    func search(_ text: String)
}

class GIFListPresenter {
    
    weak var view: GIFListViewInput!
    
    var viewModels = [GIFViewModel]()
    
    private let router: GIFListRouter.Routes
    private let service: GIFListServiceInput
    private let searchModule: GIFSearchModuleInput
    
    private var state = PaginationState(limit: 20)
    
    init(router: GIFListRouter.Routes, service: GIFListServiceInput, searchModule: GIFSearchModuleInput) {
        self.router = router
        self.service = service
        self.searchModule = searchModule
    }
}

extension GIFListPresenter: GIFListModuleInput {
    
}

extension GIFListPresenter: GIFListViewOutput {
    
    func viewIsReady() {
        viewModels = service.fetchGIFs()
        state.isEmpty = viewModels.isEmpty
        view.setFooterState(viewModels.isEmpty ? .loading : .none)
        refresh()
    }
    
    func refresh() {
        service.requestGIFs(limit: state.limit, offset: 0, force: true)
    }
    
    func didScrollToNextPage() {
        guard state.canLoadingMore else { return }
        state.isLoadingMore = true
        view.setFooterState(.loading)
        service.requestGIFs(limit: state.limit, offset: state.offset, force: false)
    }
    
    func search(_ text: String) {
        searchModule.search(text)
    }
}

extension GIFListPresenter: GIFListServiceOutput {
    
    func requestGIFs(didFinishWith gifs: [GIFViewModel], pagination: Pagination) {
        if pagination.offset == 0 {
            viewModels = gifs
        } else {
            viewModels.append(contentsOf: gifs)
        }
        state.pagination = pagination
        state.isLoadingMore = false
        state.isEmpty = viewModels.isEmpty
        
        view.reloadData()
        view.endRefreshing()
        view.setFooterState(viewModels.isEmpty ? .text : .none)
    }
    
    func requestGIFs(didFailWith error: ApiError) {
        view.scrollToLastRow(if: state.isLoadingMore)
        router.openAlertModule(error: error, handler: { self.state.isLoadingMore = false })
        view.endRefreshing()
        view.setFooterState(viewModels.isEmpty ? .text : .none)
    }
}
