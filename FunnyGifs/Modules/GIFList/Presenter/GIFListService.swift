//
//  GIFListService.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 03/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Result
import SwiftyJSON

protocol GIFListServiceInput: AnyObject {
    func fetchGIFs() -> [GIFViewModel]
    func requestGIFs(limit: Int, offset: Int, force: Bool)
}

protocol GIFListServiceOutput: AnyObject {
    func requestGIFs(didFinishWith gifs: [GIFViewModel], pagination: Pagination)
    func requestGIFs(didFailWith error: ApiError)
}

class GIFListService: GIFListServiceInput {
    
    weak var output: GIFListServiceOutput?
    
    private let apiProvider: ApiProvider<GiphyTarget>
    private let storage: Storage
    
    init(apiProvider: ApiProvider<GiphyTarget>, storage: Storage) {
        self.apiProvider = apiProvider
        self.storage = storage
    }
    
    func fetchGIFs() -> [GIFViewModel] {
        let gifs: [GIF] = storage.fetchObjects()
        return gifs.map { $0.viewModel }
    }
    
    func requestGIFs(limit: Int, offset: Int, force: Bool) {
        let completion = { [weak self] (result: Result<JSON, ApiError>) in
            switch result {
            case let .success(response):
                let data = response["data"]
                let pagination = Pagination(json: response["pagination"])
                self?.storage.storeObjects(GIF.self, from: data, force: force, offset: pagination.offset) { result in
                    switch result {
                    case let .success(viewModels):
                        self?.output?.requestGIFs(didFinishWith: viewModels, pagination: pagination)
                    case let .failure(error):
                        self?.output?.requestGIFs(didFailWith: error)
                    }
                }
            case let .failure(error):
                self?.output?.requestGIFs(didFailWith: error)
            }
        }
        apiProvider.request(.trending(limit: limit, offset: offset), completion: completion)
    }
}
