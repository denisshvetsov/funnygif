//
//  AlertRoute.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation

protocol AlertRoute {
    func openAlertModule(error: ApiError, handler: (() -> Void)?)
}

extension AlertRoute where Self: RouterProtocol {
    
    func openAlertModule(error: ApiError, handler: (() -> Void)?) {
        let module = AlertModule(error: error, handler: handler)
        viewController?.present(module.viewController, animated: true)
    }
}
