//
//  AlertModule.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

class AlertModule {
    
    let viewController: UIAlertController
    
    init(error: ApiError, handler: (() -> Void)?) {
        let title: String = "Error"
        let message: String
        switch error {
        case .connectionError:
            message = "No internet connection.\nPlease try again."
        case .serverError, .database, .parse:
            message = "Something went wrong.\nPlease try again."
        }
        viewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        viewController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { _ in handler?() }))
    }
}
