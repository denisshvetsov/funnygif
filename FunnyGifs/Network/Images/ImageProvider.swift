//
//  ImageProvider.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 13/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import YYImage
import PINCache

typealias ImageCompletion = (_ image: AnimatedImage?, _ resource: Resource) -> Void

class ImageProvider {
    
    static let shared = ImageProvider() // no time for refactor it to DI
    
    private let session: URLSession
    private let cache: PINCache
    private let downloadQueue: OperationQueue
    
    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.httpMaximumConnectionsPerHost = 10
        session = URLSession(configuration: configuration)
        cache = PINCache(name: "PINCacheFunnyGIF")
        downloadQueue = OperationQueue()
        downloadQueue.qualityOfService = .userInitiated
    }
    
    func get(with resource: Resource, completion: @escaping ImageCompletion) {
        if let frames = cache.object(forKey: resource.key) as? [ImageFrame] {
            let image = AnimatedImage.image(with: frames)
            completion(image, resource)
            return
        }
        
        if let operation = getOperation(with: resource) {
            operation.completion = completion
        } else {
            download(with: resource, completion: completion)
        }
    }
    
    func prefetch(with resource: Resource) {
        guard !cache.containsObject(forKey: resource.key) else { return }
        let operation = ImageDownloadOperation(resource: resource, session: session, cache: cache)
        downloadQueue.addOperation(operation)
    }
    
    func cancel(with resource: Resource) {
        let operation = getOperation(with: resource)
        operation?.cancel()
    }
    
    private func download(with resource: Resource, completion: @escaping ImageCompletion) {
        let operation = ImageDownloadOperation(resource: resource, session: session, cache: cache, completion: completion)
        downloadQueue.addOperation(operation)
    }
    
    private func getOperation(with resource: Resource) -> ImageDownloadOperation? {
        let operation = downloadQueue.operations.first(where: {
            guard let operation = $0 as? ImageDownloadOperation else { return false }
            return operation.resource.key == resource.key
        })
        return operation as? ImageDownloadOperation
    }
}
