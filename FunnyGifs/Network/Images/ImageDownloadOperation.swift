//
//  ImageDownloadOperation.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 15/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import YYImage
import PINCache

class ImageDownloadOperation: AsyncOperation {
    
    var completion: ImageCompletion?
    
    let resource: Resource
    private let session: URLSession
    private let cache: PINCache
    
    private var task: URLSessionDataTask?
    
    init(resource: Resource, session: URLSession, cache: PINCache, completion: ImageCompletion? = nil) {
        self.resource = resource
        self.session = session
        self.cache = cache
        self.completion = completion
        super.init()
    }
    
    override func main() {
        let resource = self.resource
        
        task = session.dataTask(with: resource.downloadURL) { data, response, error in
            
            defer { self.state = .finished }
            
            guard let data = data, let decoder = YYImageDecoder(data: data, scale: UIScreen.main.scale) else {
                guard let completion = self.completion else { return }
                DispatchQueue.main.async {
                    completion(nil, resource)
                }
                return
            }
            
            let frames: [ImageFrame] = (0..<decoder.frameCount).compactMap { i in
                guard let frameImage = decoder.frame(at: i, decodeForDisplay: true)?.image else { return nil }
                let duration = decoder.frameDuration(at: i)
                return ImageFrame(image: frameImage, duration: duration)
            }
            self.cache.setObject(frames as NSArray, forKey: resource.key)
            
            let image = AnimatedImage.image(with: frames)
            
            guard let completion = self.completion else { return }
            DispatchQueue.main.async {
                completion(image, resource)
            }
        }
        
        task?.resume()
    }
    
    override func cancel() {
        super.cancel()
        task?.cancel()
    }
}
