//
//  Resource.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 13/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation

protocol Resource {
    var key: String { get }
    var downloadURL: URL { get }
}

extension URL: Resource {
    var key: String { return path } // not `absoluteString` because host and query changing sometimes for same GIF object
    var downloadURL: URL { return self }
}
