//
//  GiphyTarget.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Moya

enum GiphyTarget {
    case trending(limit: Int, offset: Int)
    case search(_ text: String, limit: Int, offset: Int)
}

extension GiphyTarget: BaseTargetType {
    
    var baseURL: URL {
        return URL(string: "https://api.giphy.com/v1/gifs/")!
    }
    
    var path: String {
        switch self {
        case .trending:
            return "trending"
        case .search:
            return "search"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .trending, .search:
            return .get
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .trending(limit, offset):
            return ["limit": limit,
                    "offset": offset]
        case let .search(text, limit, offset):
            return ["q": text,
                    "limit": limit,
                    "offset": offset]
        }
    }
}
