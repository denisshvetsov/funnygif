//
//  BaseTargetType.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Moya

protocol BaseTargetType: TargetType {
    var parameters: [String: Any]? { get }
}

extension BaseTargetType {
    
    var task: Moya.Task {
        var requestParameters = parameters ?? [:]
        requestParameters["api_key"] = "WZf7kaJzPUhLhYTxYYaSvP5HznB6tWna"
        let encoding: ParameterEncoding
        switch self.method {
        case .get:
            encoding = URLEncoding.default
        default:
            encoding = JSONEncoding.default
        }
        return .requestParameters(parameters: requestParameters, encoding: encoding)
    }
    
    var sampleData: Data {
        let sampleURL = Bundle(for: AppDelegate.self).bundleURL.appendingPathComponent("StubResponses")
            .appendingPathComponent("\(path)")
            .appendingPathExtension("json")
        return (try? Data(contentsOf: sampleURL)) ?? Data()
    }
}
