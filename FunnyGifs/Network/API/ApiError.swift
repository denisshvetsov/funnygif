//
//  ApiError.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Moya

enum ApiError: Error {
    case connectionError
    case serverError
    case database
    case parse
    
    static func from(_ error: MoyaError) -> ApiError {
        switch error {
        case .underlying(let error, _):
            return ApiError.from(code: (error as NSError).code)
        default:
            return ApiError.serverError
        }
    }
    
    private static func from(code: Int) -> ApiError {
        switch code {
        case -1009, -1004, -1001:
            return .connectionError
        default:
            return .serverError
        }
    }
}
