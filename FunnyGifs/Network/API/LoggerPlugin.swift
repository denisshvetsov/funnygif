//
//  LoggerPlugin.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 04/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Result
import Moya

class LoggerPlugin: PluginType {
    
    func willSend(_ request: RequestType, target: TargetType) {
        let url = target.baseURL.absoluteString + target.path
        let headers = request.request?.allHTTPHeaderFields
        let baseTarget = target as? BaseTargetType
        let parameters = baseTarget?.parameters
        let logDictionary: NSDictionary = ["Headers": headers as NSDictionary? ?? "",
                                           "Parameters": parameters as NSDictionary? ?? ""]
        
        logRequest(method: target.method.rawValue,
                   fullURL: url,
                   info: logDictionary)
    }
    
    func didReceive(_ result: Result<Moya.Response, MoyaError>, target: TargetType) {
        let response = result.value?.response
        var body: String?
        if let data = result.value?.data,
            let object = try? JSONSerialization.jsonObject(with: data),
            let prettyPrintedData = try? JSONSerialization.data(withJSONObject: object, options: .prettyPrinted) {
            body = String(data: prettyPrintedData, encoding: .utf8)
        }
        let statusCode = response?.statusCode ?? 0
        let error = result.error
        // Because sometimes server return string as error
        let responseString = String(data: result.value?.data ?? Data(), encoding: .utf8)
        
        logResponse(statusCode: statusCode,
                    error: error,
                    headers: response?.allHeaderFields as NSDictionary?,
                    body: body ?? responseString ?? "empty")
    }
}

private extension LoggerPlugin {
    
    private func logRequest(method: String, fullURL: String, info: NSDictionary?) {
        print("""
            (\(method)) \(fullURL)
            Request: \(info ?? NSDictionary())
            """)
    }
    
    private func logResponse(statusCode: Int, error: Error?, headers: NSDictionary?, body: String?) {
        print("""
            Response (\(statusCode)), Error: \(error?.localizedDescription ?? "nil"):
            Headers: \(headers ?? NSDictionary())
            Body: \(body ?? "")
            """)
    }
}
