//
//  ApiProvider.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
import Result

class ApiProvider<Target> where Target: Moya.TargetType {
    
    private let provider: MoyaProvider<Target>
    
    init(endpointClosure: @escaping MoyaProvider<Target>.EndpointClosure = MoyaProvider<Target>.defaultEndpointMapping,
         requestClosure: @escaping MoyaProvider<Target>.RequestClosure = MoyaProvider<Target>.defaultRequestMapping,
         stubClosure: (MoyaProvider<Target>.StubClosure)? = nil,
         manager: Manager = MoyaProvider<Target>.defaultAlamofireManager(),
         plugins: [PluginType] = [],
         trackInflights: Bool = false) {
        
        var defaultPlugins: [PluginType] = [LoggerPlugin()]
        
        defaultPlugins.append(contentsOf: plugins)
        
        provider = MoyaProvider(endpointClosure: endpointClosure,
                                requestClosure: requestClosure,
                                stubClosure: stubClosure ?? MoyaProvider.neverStub,
                                manager: manager,
                                plugins: defaultPlugins,
                                trackInflights: trackInflights)
    }
    
    @discardableResult
    func request(_ target: Target,
                 completion: @escaping (_ result: Result<JSON, ApiError>) -> Void) -> Cancellable {
        return provider.request(target) { result in
            switch result {
            case .success(let value):
                if let json = try? JSON(data: value.data) {
                    completion(Result<JSON, ApiError>(value: json))
                } else {
                    completion(Result<JSON, ApiError>(error: ApiError.parse))
                }
            case .failure(let error):
                completion(Result<JSON, ApiError>(error: ApiError.from(error)))
            }
        }
    }
}
