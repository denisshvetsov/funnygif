//
//  AppDelegate.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 30/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Storage.setup()
        setupUI()
        return true
    }
    
    private func setupUI() {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let module = GIFListModule(transition: nil)
        let navigationController = NavigationController(rootViewController: module.viewController)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

