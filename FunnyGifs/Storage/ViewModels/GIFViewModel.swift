//
//  GIFViewModel.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

struct GIFViewModel {
    let url: URL?
    let imageFrame: CGRect
    let cellHeight: CGFloat
    
    init(gif: GIF) {
        url = URL(string: gif.medium?.url ?? "")
        guard
            let imageWidth = gif.medium?.width?.value,
            let imageHeight = gif.medium?.height?.value else {
                imageFrame = CGRect.zero
                cellHeight = 0
                return
        }
        let aspectRatio = imageWidth / imageHeight
        let width = UIScreen.main.bounds.width // assumption that tableView is fullscreen
        let height = width / aspectRatio
        
        imageFrame = CGRect(x: 0, y: 0, width: width, height: height)
        cellHeight = height + 10
    }
}

private extension String {
    
    var value: CGFloat? {
        let value = Double(self) ?? 0
        return value > 0 ? CGFloat(value) : nil
    }
}
