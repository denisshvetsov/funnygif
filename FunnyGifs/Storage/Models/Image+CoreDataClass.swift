//
//  Image+CoreDataClass.swift
//  
//
//  Created by Denis Shvetsov on 03/09/2019.
//
//

import Foundation
import CoreStore
import SwiftyJSON

@objc(Image)
public class Image: NSManagedObject {

}

extension Image: ImportableObject {
    
    public typealias ImportSource = JSON
    
    public func didInsert(from source: ImportSource, in transaction: BaseDataTransaction) throws {
        url = source["webp"].string
        width = source["width"].string
        height = source["height"].string
        size = source["size"].string
    }
}
