//
//  GIF+CoreDataClass.swift
//  
//
//  Created by Denis Shvetsov on 03/09/2019.
//
//

import Foundation
import CoreStore
import SwiftyJSON

@objc(GIF)
public class GIF: NSManagedObject {

}

extension GIF: ImportableUniqueObject {
    
    public typealias UniqueIDType = String
    public typealias ImportSource = JSON
    
    public static var uniqueIDKeyPath: String {
        return #keyPath(GIF.id)
    }
    
    public static func uniqueID(from source: ImportSource, in transaction: BaseDataTransaction) throws -> String? {
        return source["id"].string
    }
    
    public func update(from source: ImportSource, in transaction: BaseDataTransaction) throws {
        id = source["id"].string
        type = source["type"].string
        slug = source["slug"].string
        url = source["url"].string
        username = source["username"].string
        rating = source["rating"].string
        title = source["title"].string
        
        let imagesSource = source["images"]
        medium = try transaction.importObject(Into<Image>(), source: imagesSource["fixed_width"])
        original = try transaction.importObject(Into<Image>(), source: imagesSource["original"])
    }
}

extension GIF: Indexable {}

extension GIF: ViewModelRepresentable {
    
    typealias ViewModel = GIFViewModel
    
    var viewModel: GIFViewModel {
        return GIFViewModel(gif: self)
    }
}
