//
//  Storage.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import CoreStore
import SwiftyJSON

class Storage {
    
    static let noIndex: Int64 = -1 // can't use optional Int in CoreData
    
    static func setup() {
        CoreStore.defaultStack = DataStack(xcodeModelName: "Model")
        do {
            try CoreStore.addStorageAndWait(
                SQLiteStore(
                    fileName: "Model.sqlite",
                    localStorageOptions: .recreateStoreOnModelMismatch
                )
            )
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func storeObject<Object: ImportableUniqueObject & ViewModelRepresentable>(
        _ type: Object.Type,
        from json: JSON,
        completion: @escaping (_ result: Result<Object.ViewModel, ApiError>) -> Void)
        where Object.ImportSource == JSON {
            
            CoreStore.perform(asynchronous: { transaction -> Object.ViewModel in
                do {
                    guard let item = try transaction.importUniqueObject(Into<Object>(), source: json) else {
                        throw ApiError.database
                    }
                    return item.viewModel
                }
            }, success: { object in
                completion(.success(object))
            }, failure: { _ in
                completion(.failure(.database))
            })
    }
    
    func storeObjects<Object: ImportableUniqueObject & ViewModelRepresentable & Indexable & Hashable>(
        _ type: Object.Type,
        from json: JSON,
        force: Bool,
        offset: Int? = nil,
        completion: @escaping (_ result: Result<[Object.ViewModel], ApiError>) -> Void)
        where Object.ImportSource == JSON {
            
            CoreStore.perform(asynchronous: { transaction -> [Object.ViewModel] in
                do {
                    let items = try transaction.importUniqueObjects(Into<Object>(), sourceArray: json.arrayValue)
                    
                    if force {
                        let allItems = try transaction.fetchAll(From<Object>())
                        var allItemsSet = Set<Object>(allItems)
                        let itemsSet = Set<Object>(items)
                        allItemsSet.subtract(itemsSet)
                        let unusedItems = Array(allItemsSet)
                        transaction.delete(unusedItems)
                    }
                    
                    guard let offset = offset else { return items.map { $0.viewModel } }
                    
                    var viewModels = [Object.ViewModel]()
                    for (index, var item) in items.enumerated() {
                        item.index = Int64(index + offset)
                        viewModels.append(item.viewModel)
                    }
                    
                    return viewModels
                }
            }, success: { viewModels in
                completion(.success(viewModels))
            }, failure: { _ in
                completion(.failure(.database))
            })
    }
    
    func fetchObject<Object: ImportableUniqueObject>(id: Object.UniqueIDType) -> Object? {
        return try? CoreStore.fetchOne(From<Object>().where(format: "\(Object.uniqueIDKeyPath) == %@", id))
    }
    
    func fetchObjects<Object: ImportableUniqueObject & Indexable>() -> [Object] {
        let objects = try? CoreStore.fetchAll(
            From<Object>()
                .where(format: "index != %i", Storage.noIndex)
                .orderBy(.ascending("index"))
        )
        return objects ?? []
    }
}
