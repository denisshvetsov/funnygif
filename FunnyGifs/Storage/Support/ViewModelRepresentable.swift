//
//  ViewModelRepresentable.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

protocol ViewModelRepresentable {
    associatedtype ViewModel
    var viewModel: ViewModel { get }
}
