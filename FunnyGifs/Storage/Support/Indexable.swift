//
//  Indexable.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

protocol Indexable {
    var index: Int64 { get set }
}
