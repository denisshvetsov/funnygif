//
//  Pagination.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 06/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import SwiftyJSON

struct Pagination {
    let totalCount: Int
    let count: Int
    let offset: Int
    
    init(json: JSON) {
        totalCount = json["total_count"].intValue
        count = json["count"].intValue
        offset = json["offset"].intValue
    }
}
