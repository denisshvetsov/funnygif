//
//  ModalTransition.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 31/08/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

class ModalTransition: NSObject {
    
    let animated: Bool
    
    weak var viewController: UIViewController?
    
    init(animated: Bool) {
        self.animated = animated
    }
}

extension ModalTransition: Transition {
    
    func open(_ viewController: UIViewController) {
        self.viewController?.present(viewController, animated: animated)
    }
    
    func close(_ viewController: UIViewController) {
        viewController.dismiss(animated: animated)
    }
}
