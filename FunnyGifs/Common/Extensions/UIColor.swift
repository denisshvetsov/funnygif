//
//  UIColor.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 15/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

extension UIColor {
    static let translucentBlack = UIColor(white: 0.07, alpha: 1.0)
}
