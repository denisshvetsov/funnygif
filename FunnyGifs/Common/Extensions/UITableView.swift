//
//  UITableView.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 15/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

extension UITableView {
    
    func setSmoothScroll() {
        self.estimatedRowHeight = 0
        self.estimatedSectionHeaderHeight = 0
        self.estimatedSectionFooterHeight = 0
    }
}
