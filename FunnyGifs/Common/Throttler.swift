//
//  Throttler.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 11/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import Foundation

class Throttler {
    
    private let delay: TimeInterval
    private let queue: DispatchQueue
    private var workItem = DispatchWorkItem(block: {})
    private var previousRun = Date.distantPast
    
    private(set) var isActive: Bool = false
    
    init(delay: TimeInterval, queue: DispatchQueue = DispatchQueue.main) {
        self.delay = delay
        self.queue = queue
    }
    
    func throttle(block: @escaping () -> Void) {
        workItem.cancel()
        workItem = DispatchWorkItem { [weak self] in
            self?.previousRun = Date()
            block()
            self?.isActive = false
        }
        isActive = true
        let queueDelay = previousRun.timeIntervalSinceNow > delay ? 0 : delay
        queue.asyncAfter(deadline: .now() + Double(queueDelay), execute: workItem)
    }
    
    func cancel() {
        workItem.cancel()
        workItem = DispatchWorkItem(block: {})
        isActive = false
    }
}
