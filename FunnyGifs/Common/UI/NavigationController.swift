//
//  NavigationController.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 15/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.barTintColor = .black
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
}
