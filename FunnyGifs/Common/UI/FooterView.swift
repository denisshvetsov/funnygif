//
//  FooterView.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 14/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit

class FooterView: UIView {
    
    enum State {
        case loading
        case text
        case none
    }
    
    var state: State = .none {
        didSet {
            switch state {
            case .loading:
                textLabel.isHidden = true
                activityIndicatorView.startAnimating()
            case .text:
                textLabel.isHidden = false
                activityIndicatorView.stopAnimating()
            case .none:
                textLabel.isHidden = true
                activityIndicatorView.stopAnimating()
            }
        }
    }
    
    private let activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .white)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicatorView
    }()
    
    private let textLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.textColor = .lightGray
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
    
    init(frame: CGRect, text: String? = nil) {
        super.init(frame: frame)
        self.textLabel.text = text
        initSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSetup()
    }
    
    private func initSetup() {
        addSubview(textLabel)
        addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        NSLayoutConstraint.activate([
            textLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            activityIndicatorView.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
        state = .none
    }
}
