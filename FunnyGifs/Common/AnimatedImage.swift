//
//  AnimatedImage.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 11/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

import UIKit
import YYImage

class ImageFrame: NSObject, NSCoding {
    let image: UIImage?
    let duration: TimeInterval
    
    enum Key: String {
        case image
        case duration
    }
    
    init(image: UIImage?, duration: TimeInterval) {
        self.image = image
        self.duration = duration
    }
    
    convenience required init?(coder aDecoder: NSCoder) {
        let data = aDecoder.decodeObject(forKey: Key.image.rawValue) as? Data
        let duration = aDecoder.decodeDouble(forKey: Key.duration.rawValue)
        let image = UIImage(data: data ?? Data())
        self.init(image: image, duration: duration)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(image?.jpegData(compressionQuality: 0.75), forKey: Key.image.rawValue)
        aCoder.encode(duration, forKey: Key.duration.rawValue)
    }
}

class AnimatedImage: UIImage, YYAnimatedImage {
    
    static func image(with frames: [ImageFrame]) -> AnimatedImage {
        let image = AnimatedImage()
        image.frames = frames
        return image
    }
    
    var frames = [ImageFrame]()
    
    func animatedImageFrameCount() -> UInt {
        return UInt(frames.count)
    }
    
    func animatedImageLoopCount() -> UInt {
        return 0 // infinity
    }
    
    func animatedImageBytesPerFrame() -> UInt {
        return 0
    }
    
    func animatedImageFrame(at index: UInt) -> UIImage? {
        return frames[Int(index)].image
    }
    
    func animatedImageDuration(at index: UInt) -> TimeInterval {
        return frames[Int(index)].duration
    }
}
