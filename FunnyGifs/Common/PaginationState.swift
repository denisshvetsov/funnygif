//
//  PaginationState.swift
//  FunnyGifs
//
//  Created by Denis Shvetsov on 15/09/2019.
//  Copyright © 2019 Denis Shvetsov. All rights reserved.
//

struct PaginationState {
    
    let limit: Int
    
    var isLoadingMore = false
    var pagination: Pagination?
    var isEmpty: Bool = true
    
    var canLoadingMore: Bool {
        return !isLoadingMore && isObjectsLeft && !isEmpty
    }
    
    var offset: Int {
        guard let pagination = pagination else { return 0 }
        return pagination.offset + limit
    }
    
    private var isObjectsLeft: Bool {
        guard let pagination = pagination else { return false }
        return pagination.offset + pagination.count < pagination.totalCount
    }
    
    init(limit: Int) {
        self.limit = limit
    }
    
    mutating func reset() {
        isLoadingMore = false
        pagination = nil
        isEmpty = true
    }
}
