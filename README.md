# FunnyGIF

Simple GIF app powered by Giphy API

### Описание

Необходимо реализовать приложение с возможностью просмотра контента из giphy.com. 
Приложение должно поддерживать пагинацию и в каком-то виде кеширование загруженного контента. 
Также нужно реализовать возможность поиска контента по библиотеке giphy.

### Библиотеки

- **CoreStore**: кэширование GIF объектов в Core Data
- **SwiftyJSON**: парсинг, ImportSource для CoreStore объектов
- **Moya**: работа с сетью
- **YYImage/WebP**: декодинг WebP изображений и отображение в YYAnimatedImageView
- **PINCache**: кэширование изображений

### Превью

<img src="preview.gif" width="300">
